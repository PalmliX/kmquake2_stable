This is the source code to the 3ZB2 support DLL for KMQuake2 .
It also supports the model_train, model_spawn, and rotating func_train entities from the Lazarus mod.

Note this source code is under the original "Limited Program Source Code License" from id Software,
NOT the GPL.  It should not be mixed with GPLed code.
